﻿var app = angular.module("myApp", ['myController', 'utilServices', 'myDirective', 'ui.bootstrap', 'ui.router', 'webApiService','cwLeftMenu','ngGrid']);
var controllers = angular.module("myController", []);
var directives = angular.module("myDirective", []);


app.config(["$stateProvider", "$urlRouterProvider", "$httpProvider", function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $httpProvider.defaults.headers.post['X-CSRFToken'] = $("#csrf").val();
    $urlRouterProvider.otherwise("/");//默认展示页面
    $stateProvider.state('home', {
        url: "/",
        controller: "home",
        templateUrl: static_url + "client/views/home.html"
    }).state('test', {
        url: "/test",
        controller: "test",
        templateUrl: static_url + "client/views/test.html"
    }).state('task_template', {
        url: "/task_template",
        controller: "task_template",
        templateUrl: static_url + "client/views/test1/task_template.html"
    }).state('host_detail', {
        url: "/host_detail?id",
        controller: "host_detail",
        templateUrl: static_url + "client/views/test1/host_detail.html"
    }).state('chart', {
        url: "/chart",
        controller: "chart",
        templateUrl: static_url + "client/views/frame/chart.html"
    }).state('hostList', {
        url: "/hostList",
        controller: "hostList",
        templateUrl: static_url + "client/views/frame/hostList.html"
    }).state('host', {
        url: "/host",
        controller: "host",
        templateUrl: static_url + "client/views/host/host.html"
    }).state('hostStatus', {
        url: "/hostStatus",
        controller: "hostStatus",
        templateUrl: static_url + "client/views/host/hostStatus.html"
    }).state('chart_demo', {
        url: "/chart_demo",
        controller: "chart_demo",
        templateUrl: static_url + "client/views/frame/chart_demo.html"
    }).state('backOption', {
        url: "/backOption",
        controller: "backOption",
        templateUrl: static_url + "client/views/exam3/backOption.html"
    }).state('backRecord', {
        url: "/backRecord",
        controller: "backRecord",
        templateUrl: static_url + "client/views/exam3/backRecord.html"
    })

}]);
