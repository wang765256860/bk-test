controllers.controller("backOption", function ($scope, bizService, backService, $modal, loading, $filter, msgModalN, msgModal, errorModal, confirmModal) {

    $scope.app_id1 = '';
    $scope.appOption1 = {
        data: "app_list",
        multiple: false,
        modelData: "app_id1"
    };
    $scope.allServer = [];
    $scope.serverList = [];
    $scope.filterObj = {value: ''};
    $scope.attrList = [];
    $scope.select_attr = {value: ''};
    $scope.args = {
        ip: '',
        menu: '/data/logs',
        filename: 'txt',
        ip_list: [],
    };

    $scope.get_all_biz = function () {
        loading.open();
        bizService.get_all_biz({}, {}, function (res) {
            loading.close();
            if (res.result) {
                $scope.app_list = res.data;
                $scope.app_id1 = res.data[0].id;
                $scope.changeApp1();
            }
        })
    };

    $scope.get_all_biz();

    // 切换业务查询主机，以及topo树
    $scope.changeApp1 = function () {
        $scope.searchServer({bk_obj_id: 'biz', bk_inst_id: $scope.app_id1});
        bizService.get_app_topo({app_id: $scope.app_id1}, {}, function (res) {
            $scope.businessTopo = res.data;
        })
    };

    // 查询主机所有属性
    $scope.searchAttr = function () {
        bizService.get_host_attr({}, {}, function (res) {
            $scope.attrList = res.data;
        })
    };
    $scope.searchAttr();

    // 树形topo组件
    $scope.zTreeOptions = {
        check: {
            enable: false
        },
        data: {
            key: {
                // 显示的字段
                name: "bk_inst_name",
                children: "child",
                isParent: "isParent"
            }
        },
        onClick: function (event, treeId, treeNode) {
            $scope.searchServer(treeNode)
        }
    };

    // 根据点击树形节点或者切换业务查询所有的主机
    $scope.searchServer = function (treeNode) {
        loading.open();
        bizService.search_server_list({
            bk_biz_id: $scope.app_id1
        }, {
            bk_obj_id: treeNode.bk_obj_id,
            value: treeNode.bk_inst_id
        }, function (res) {
            loading.close();
            $scope.allServer = angular.copy(res.data);
            $scope.args.ip = res.ipList
            // $scope.filterServer();
        })
    };

    // 根据属性过滤主机，不重新查询接口 ，$filter
    // $scope.filterServer = function () {
    //     loading.open();
    //     if ($scope.select_attr.value === '') {
    //         $scope.serverList = angular.copy($scope.allServer);
    //         loading.close();
    //     }
    //     else {
    //         // 循环主机列表，有该属性值的主机则返回
    //         $scope.serverList = $filter('filter')($scope.allServer, function (i) {
    //             loading.close();
    //             if (i[$scope.select_attr.value] == undefined || i[$scope.select_attr.value] == null)
    //                 return false;
    //             return i[$scope.select_attr.value].indexOf($scope.filterObj.value) > -1
    //         })
    //     }
    // };


    // ServerList
    $scope.gridOption = {
        data: 'allServer',
        columnDefs: [
            {
                field: '',
                displayName: '序号',
                cellTemplate: '<div class="ngCellText ng-scope col1 colt1" ng-class="col.colIndex()"><span ng-cell-text="">{{row.rowIndex + 1}}</span></div>'
            },
            {field: 'bk_host_innerip', displayName: 'IP'},
            {field: 'fileList', displayName: '文件列表'},
            {field: 'fileNum', displayName: '文件数量'},
            {field: 'fileSize', displayName: '文件总大小'},
            {
                displayName: '操作', width: 180,
                cellTemplate: '<div style="width:100%;text-align: center;padding-top: 5px;z-index: 1">' +
                    '<span ng-click="backup(row.entity)" class="label label-primary button-radius" style="min-width:50px;cursor:pointer;">立即备份</span>' +
                    '</div>'
            }
        ]
    };

    $scope.search = function () {
        loading.open();
        for (var i = 0; i < $scope.allServer.length; i++) {
            $scope.args.ip_list.push({
                'ip': $scope.allServer[i].bk_host_innerip,
                'bk_cloud_id': $scope.allServer[i].bk_cloud_id[0].bk_inst_id
            })
        }
        backService.execute_get_file_info({'bk_biz_id': $scope.app_id1}, $scope.args, function (res) {
            loading.close();
            if (res.result) {
                $scope.return_data = res.data;
                for (let j = 0; j < $scope.return_data.length; j++) {
                    for (let m = 0; m < $scope.allServer.length; m++) {
                        if ($scope.return_data[j].ip == $scope.allServer[m].bk_host_innerip) {
                            $scope.allServer[m].fileList = $scope.return_data[j].file_name;
                            $scope.allServer[m].fileNum = $scope.return_data[j].num;
                            $scope.allServer[m].fileSize = $scope.return_data[j].size;

                        }
                    }
                }
            }
        })
    };

    $scope.backup = function (row) {
        loading.open();
        backService.execute_backup({}, {
            'ip': row.bk_host_innerip,
            'file_list': row.fileList,
            'file_num': row.fileNum,
            'file_size': row.fileSize,
        }, function (res) {
            loading.close();
            if (res.result) {
                msgModal.open('success', '备份成功');
            }
        })
    }

});
