controllers.controller("backRecord", function ($scope, bizService, backService, $modal, loading, $filter, msgModalN, msgModal, errorModal, confirmModal) {


    $scope.pagingOptions = {
        pageSizes: [5, 10, 15, 20],
        pageSize: "10",
        currentPage: 1
    };
    $scope.args = {};

    $scope.rowSection = [];
    $scope.all_len;
    $scope.maxP;
    $scope.backupList = [];

    $scope.search = function (page_size, current_page) {
        loading.open();
        page_size = page_size || $scope.pagingOptions.pageSize;
        current_page = current_page || $scope.pagingOptions.currentPage;
        $scope.args.pageOption = {
            pageSize: page_size,
            pageNum: current_page
        };
        backService.get_backup_record({}, $scope.args, function (res) {
            loading.close();
            if (res.result) {
                $scope.backupList = res.data
                $scope.maxP = res.pageNum;
                $scope.all_len = res.allLen;
            } else {
                msgModal.open('error', res.message)
            }
        })
    };

    $scope.search();

    // ServerList
    $scope.gridOption = {
        enableColumnResize: true,
        multiSelect: true,
        enableRowSelection: true,
        showSelectionCheckbox: true,
        data: 'backupList',
        selectedItems: $scope.rowSection,
        enablePaging: true,
        pagingOptions: $scope.pagingOptions,
        totalServerItems: 'totalSerItems',
        selectWithCheckboxOnly: true,
        columnDefs: [
            {
                field: '',
                displayName: '序号',
                cellTemplate: '<div class="ngCellText ng-scope col1 colt1" ng-class="col.colIndex()"><span ng-cell-text="">{{row.rowIndex + 1}}</span></div>'
            },
            {field: 'ip', displayName: 'IP'},
            {field: 'file_list', displayName: '文件列表'},
            {field: 'file_num', displayName: '文件数量'},
            {field: 'file_size', displayName: '文件大小'},
            {field: 'when_backup', displayName: '备份时间'},
            {field: 'back_man', displayName: '备份人'},
            {
                displayName: 'JOB链接', width: 180,
                cellTemplate: '<div style="width:100%;text-align: center;padding-top: 5px;z-index: 1">' +
                    '<span ng-click="opendetail(row.entity)" class="label label-primary button-radius" style="min-width:50px;cursor:pointer;">查看详情</span>' +
                    '</div>'
            }
        ]
    };

    $scope.goToPage = function (page) {
        if (page == 0 && $scope.pagingOptions.currentPage != 1) {
            $scope.pagingOptions.currentPage = 1;
        } else if (page == -1 && $scope.pagingOptions.currentPage != 1) {
            $scope.pagingOptions.currentPage = $scope.pagingOptions.currentPage - 1;
        } else if (page == 1 && $scope.pagingOptions.currentPage != $scope.maxP) {
            $scope.pagingOptions.currentPage = $scope.pagingOptions.currentPage + 1;
        } else if (page == 9 && $scope.pagingOptions.currentPage != $scope.maxP) {
            $scope.pagingOptions.currentPage = $scope.maxP;
        } else if (page.num != undefined)
            $scope.pagingOptions.currentPage = page.num;
    };

    $scope.$watch("pagingOptions.currentPage", function (e) {
        if ($scope.maxP)
            $scope.search($scope.pagingOptions.pageSize, e);
    });
    $scope.$watch("pagingOptions.pageSize", function (e) {
        if ($scope.maxP)
            $scope.search(e, $scope.pagingOptions.currentPage);
    });


    $scope.opendetail = function (row) {
        alert('暂时看不了');
    }


});
