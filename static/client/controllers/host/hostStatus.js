controllers.controller("hostStatus", ['$scope', 'msgModal', 'loading', 'errorModal', 'sysService', 'hostService',
    function ($scope, msgModal, loading, errorModal, sysService, hostService) {

    $scope.args = {
        'business': '',
        'ip': '',
    };

    $scope.get_biz = function () {
        sysService.get_all_biz({}, {}, function (res) {
            if (res.result) {
                console.log("business:", res.data);
                $scope.business = res.data;
            } else {
                msgModal.open("error", res.message);
            }
        })
    };
    $scope.get_biz();

    $scope.get_host_by_biz = function () {
        loading.open();
        hostService.get_host_by_biz({'biz_id': $scope.args.business}, {'ip': $scope.args.ip}, function (res) {
            loading.close();
            $scope.hostList = res.data;
        })
    };

    // $scope.get_host_by_biz();

    // ServerList
    $scope.gridOption = {
        data: 'hostList',
        columnDefs: [
            {field: 'inner_ip', displayName: '内网IP'},
            {field: 'os', displayName: '系统名'},
            {field: 'host_name', displayName: '主机名'},
            {field: 'cloud_area_name', displayName: '云区域'},
            {field: 'mem', displayName: 'Mem(%)'},
            {field: 'disk', displayName: 'Disk(%)'},
            {field: 'cpu', displayName: 'CPU(%)'},
            {
                displayName: '操作', width: 180,
                cellTemplate: '<div style="width:100%;text-align: center;padding-top: 5px;z-index: 1">' +
                '<span ng-click="searchHost(row.entity)" class="label label-primary button-radius" style="min-width:50px;cursor:pointer;">查询</span>' +
                '</div>'
            }
        ]
    };

    var change_host_info = function(ip, mem, disk, cpu) {
        for (var i=0;i<$scope.hostList.length;i++) {
            if ($scope.hostList[i]['inner_ip'] == ip) {
                $scope.hostList[i]['mem'] = mem;
                $scope.hostList[i]['disk'] = disk;
                $scope.hostList[i]['cpu'] = cpu;
            }
        }
    };

    $scope.searchHost = function (row) {
        loading.open();
        hostService.excute_search_script({},
            {'biz_id': $scope.args.business,
                'ip': row.inner_ip,
                'bk_cloud_id': row.cloud_area_id
            }, function (res) {
            if (res.result) {
                loading.close();
                change_host_info(row.inner_ip, res.data.mem, res.data.disk, res.data.cpu)
            }
        })
    }



}]);