controllers.controller("host", ["$scope", "$modal", "loading", "msgModalN", "msgModal", "errorModal", "confirmModal", "sysService", "$filter",
    function ($scope, $modal, loading, msgModalN, msgModal, errorModal, confirmModal, sysService, $filter) {

        $scope.args = {
            'name': '',
        };

        // $scope.search = function () {
        //     loading.open();
        //     sysService.get_host_info1({}, {}, function (res) {
        //         loading.close();
        //         if (res.result) {
        //             $scope.host_data = res.data
        //         }
        //     })
        // };
        //
        // $scope.search();
        //
        //
        // // ServerList
        // $scope.gridOption = {
        //     data: 'host_data',
        //     columnDefs: [
        //         {field: 'biz', displayName: '业务'},
        //         {field: 'set', displayName: '集群'},
        //         {field: 'host', displayName: 'ip'}
        //     ]
        // };

        $scope.add = function () {
            var modalInstance = $modal.open({
                templateUrl: static_url + 'client/views/host/addhost.html',
                windowClass: 'dialogForm',
                controller: 'addhost',
                backdrop: 'static',
                resolve: {
                    objectItem: function () {
                        return angular.copy({});
                    }
                }
            });
        };
    }]);
