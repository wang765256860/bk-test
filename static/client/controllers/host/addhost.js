controllers.controller('addhost', function ($scope, $modalInstance, msgModal, loading, errorModal, objectItem, sysService) {

    $scope.args = {
        business: "",
        set: "",
        host: ""
    };
    $scope.confirm = function () {

    };

    $scope.initBusiness = function () {
        sysService.get_all_biz({}, {}, function (res) {
            if (res.result) {
                console.log("business:", res.data);
                $scope.business = res.data;
            } else {
                msgModal.open("error", res.message);
            }
        })
    };
    $scope.initBusiness();

    $scope.get_set = function(biz) {
        sysService.get_app_topo({app_id: $scope.args.business}, {}, function (res) {
            $scope.setList = res.data[0]['child'];
        })
    };

    $scope.get_host = function(){
        sysService.search_server_list({
            bk_biz_id: $scope.args.business
        }, {
            bk_obj_id: 'set',
            value: $scope.args.set
        }, function (res) {
            $scope.allServer = angular.copy(res.data);

        })
    };

    $scope.confirm = function() {
        loading.open();
        sysService.add_host_info({}, $scope.args, function (res) {
            loading.close();
            if (res.result){
                msgModal.open("success", "导入成功");
                $modalInstance.close();
            }
        })
    };

    $scope.cancel = function () {
        $modalInstance.dismiss("cancel");
    };

});