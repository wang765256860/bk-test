controllers.controller("site", ["$scope",
    function ($scope) {
        $scope.menuList = [
            // {
            //     displayName: "test", children: [
            //         {displayName: "test", url: "#/test"},
            //     ]
            // },
            // {
            //     displayName: "主机资源", children: [
            //         {displayName: "常用组件", url: "#/hostList"},
            //     ]
            // },
            // {
            //     displayName: "图", children: [
            //         {displayName: "图", url: "#/chart"},
            //         {displayName: "demo图", url: "#/chart_demo"},
            //     ]
            // },
            // {
            //     displayName: "主机列表", children: [
            //         {displayName: "主机列表", url: "#/host"},
            //     ]
            // },
            // {
            //     displayName: "主机状态", children: [
            //         {displayName: "主机状态", url: "#/hostStatus"},
            //     ]
            // },
            {
                displayName: "首页", url: "#/home"
            },
            {
                displayName: "备份操作", url: "#/backOption"
            },
            {
                displayName: "备份记录", url: "#/backRecord"
            },
        ];
        $scope.menuOption = {
            data: $scope.menuList
        };
    }]);

