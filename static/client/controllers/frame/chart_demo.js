controllers.controller("chart_demo", function ($scope, $modal, loading, msgModalN, msgModal, errorModal, confirmModal, sysService, $filter) {

    $scope.piechart = {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false,
            type: 'pie',
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: true,
                    distance: -50,
                    style: {
                        fontWeight: 'bold',
                        color: 'white'
                    }
                },
                name: 'browser',
                innerSize: '80%',
                startAngle: 0,
                endAngle: 0,
                center: ['50%', '40%'],
                size: '110%'
            }
        },
        series: [{
            data: [
                ['Chrome', 58.9],
                ['Firefox', 13.29],
                ['Internet Explorer', 13],
                ['Edge', 3.78],
                ['Safari', 3.42],
                {
                    name: 'Other',
                    y: 7.61,
                    dataLabels: {
                        enabled: false
                    }
                }
            ]
        }]
    };

    $scope.pieList = [
        {y: 3, color: "#ea4335", name: "高"},
        {y: 2, color: "#fbbc05", name: "中"},
        {y: 1, color: "#f5fc88", name: "低"},
    ]

    $scope.piechart = {
        data: "pieList",
        title: {text: '哈哈哈哈', enabled: true},
        unit: "",
        size: "200px"
    };
});