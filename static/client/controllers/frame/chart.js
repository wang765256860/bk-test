controllers.controller("chart", ["$scope", "$modal", "loading", "msgModalN", "msgModal", "errorModal", "confirmModal", "sysService", "$filter",
    function ($scope, $modal, loading, msgModalN, msgModal, errorModal, confirmModal, sysService, $filter) {

        // 时间折线图
        function initTimes() {
            var date = new Date();
            var result = [];
            for (var i = 0; i < 200; i++) {
                var datetime = new Date(date.getTime() - i * 3600 * 1000).getTime();
                // var rate = Math.random();
                var rate = Math.random();
                result.push([datetime, rate]);
            }
            return result;
        }

        $scope.lineSeries = []
        var theme = ["cpu", "disk", "memory"]

        theme.forEach(function (value) {
            $scope.lineSeries.push({name: value, data: initTimes()})
        })


        $scope.lineData = {
            tickInterval: 24 * 3600 * 1000,
            series: $scope.lineSeries
        };


        $scope.lineChart = {
            chart: {type: 'line'},
            yAxis: {
                labels: {
                    format: '{value}%'
                }
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: {
                    second: '%M:%S',
                    minute: '%M:%S',
                    hour: '%H:%M:%S',
                    day: '%Y-%m-%d'
                },
                tickInterval: 1 * 3600 * 1000
            },
            //提示框位置和显示内容
            tooltip: {
                crosshairs: [{
                    width: 1,
                    color: 'red'
                }],
                xDateFormat: '%m-%d %H:%M:%S',
                shared: true,
            },
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom',
                borderWidth: 0
            },
            series: [],
            watchData: "lineData"
        };

        // 普通折线图

    }])