﻿services = angular.module('webApiService', ['ngResource', 'utilServices']);

//生产代码
var POST = "POST";
var GET = "GET";

//测试代码
//var sourceRoute = "./Client/MockData";
//var fileType = ".html";
//var POST = "GET";
//var GET = "GET";
services.factory('sysService', ['$resource', function ($resource) {
    return $resource(site_url + ':actionName/', {},
        {
            get_app_list: {method: POST, params: {actionName: 'get_app_list'}, isArray: false},
            get_task_model: {method: POST, params: {actionName: 'get_task_model'}, isArray: false},
            delete_task_template: {method: POST, params: {actionName: 'delete_task_template'}, isArray: false},
            update_task_template: {method: POST, params: {actionName: 'update_task_template'}, isArray: false},
            get_all_biz: {method: POST, params: {actionName: 'get_all_biz'}, isArray: false},
            search_server_list: {method: POST, params: {actionName: 'search_server_list'}, isArray: false},
            get_app_topo: {method: POST, params: {actionName: 'get_app_topo'}, isArray: false},
            add_host_info: {method: POST, params: {actionName: 'add_host_info'}, isArray: false},
            get_host_info1: {method: POST, params: {actionName: 'get_host_info1'}, isArray: false},
        });
}]);

services.factory('bizService', ['$resource', function ($resource) {
    return $resource(site_url + ':actionName/', {},
        {
            get_all_biz: {method: POST, params: {actionName: 'get_all_biz'}, isArray: false},
            search_server_list: {method: POST, params: {actionName: 'search_server_list'}, isArray: false},
            get_app_topo: {method: POST, params: {actionName: 'get_app_topo'}, isArray: false},
            get_host_attr: {method: POST, params: {actionName: 'get_host_attr'}, isArray: false},
        });
}]);

services.factory('hostService', ['$resource', function ($resource) {
    return $resource(site_url + ':actionName/', {},
        {
            get_all_biz: {method: POST, params: {actionName: 'get_all_biz'}, isArray: false},
            get_host_by_biz: {method: POST, params: {actionName: 'get_host_by_biz'}, isArray: false},
            excute_search_script: {method: POST, params: {actionName: 'excute_search_script'}, isArray: false},
        });
}])

services.factory('backService', ['$resource', function ($resource) {
    return $resource(site_url + ':actionName/', {},
        {
            execute_get_file_info: {method: POST, params: {actionName: 'execute_get_file_info'}, isArray: false},
            execute_backup: {method: POST, params: {actionName: 'execute_backup'}, isArray: false},
            get_backup_record: {method: POST, params: {actionName: 'get_backup_record'}, isArray: false},

        });
}])


;//这是结束符，请勿删除