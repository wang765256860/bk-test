# -*- coding: utf-8 -*-
from django.http import HttpResponse, JsonResponse
from blueking.component.shortcuts import get_client_by_request, get_client_by_user
import json
import requests
from conf.default import *

#
#
# def get_all_biz():
#     url = BK_PAAS_HOST + '/api/c/compapi/v2/cc/search_business/'
#     params = {'bk_app_code': APP_ID,
#               'bk_app_secret': APP_TOKEN,
#               'bk_username': 'admin',
#               'fields': ['bk_biz_id', 'bk_biz_name'],
#               'page': {'limit': 200, 'start': 0}}
#     r = requests.post(url, json=params, verify=False)
#     data = json.loads(r.content)
#     print data
#
#
# def get_host():
#     url = BK_PAAS_HOST + '/api/c/compapi/v2/cc/search_host/'
#     params = {'bk_app_code': APP_ID,
#               'bk_app_secret': APP_TOKEN,
#               'bk_username': 'admin',
#               'condition': [
#                   {"bk_obj_id": "biz",
#                    "fields": [],
#                    "condition": [{
#                        "field": 'bk_biz_id',
#                        'operator': '$eq',
#                        'value': 2
#                    }]}
#               ],
#               'page': {'limit': 200, 'start': 0}
#               }
#     r = requests.post(url, json=params, verify=False)
#     data = json.loads(r.content)
#     print data
#
#
# get_host()
#
# # -*- coding: utf-8 -*-
# from common.mymako import render_json
# from conf.default import BK_PAAS_HOST, APP_ID, APP_TOKEN
# from conf.default import RUN_MODE
# import requests
# import json
# import os
# import datetime
#
#
# # 根据用户权限获取业务
# def get_biz_by_user(user):
#     url = BK_PAAS_HOST + '/api/c/compapi/v2/cc/search_business/'
#     params = {'bk_app_code': APP_ID,
#               'bk_app_secret': APP_TOKEN,
#               'bk_username': user,
#               'fields': ['bk_biz_id', 'bk_biz_name'],
#               'page': {'limit': 200, 'start': 0}}
#     try:
#         r = requests.post(url, json=params)
#         biz_data = json.loads(r.content)
#         if biz_data['result']:
#             data = biz_data['data']
#             return {'result': True, 'data': data}
#         else:
#             message = biz_data['message']
#             return {'result': False, 'message': message}
#     except Exception, e:
#         return {'result': False, 'message': e.message}
#
#
# def get_biz_topology_by_id(user, biz_id):
#     url = BK_PAAS_HOST + "/api/c/compapi/v2/cc/search_biz_inst_topo/"
#     params = {
#         'bk_app_code': APP_ID,
#         'bk_app_secret': APP_TOKEN,
#         'bk_username': user,
#         'bk_biz_id': biz_id
#     }
#     try:
#         data = requests.post(url, json=params)
#         biz_data = json.loads(data.content)
#         if biz_data['result']:
#             return {'result': True, 'data': biz_data['data']}
#         else:
#             return {'result': False, 'message': biz_data['message']}
#     except Exception, e:
#         return {'result': False, 'message': e.message}
#
#
# def get_biz_topology(requests):
#     user = requests.user.username
#     biz_list = get_biz_by_user(user)
#     if biz_list['result']:
#         for i in biz_list['data']['info']:
#             topology = get_biz_topology_by_id(user, i['bk_biz_id'])
#             if topology['result']:
#                 i['topology'] = topology['data']
#                 i['topo_reslut'] = True
#             else:
#                 i['topology'] = []
#                 i['topo_reslut'] = False
#         return render_json({'result': True, 'data': biz_list['data']})
#     else:
#         return render_json({'result': False, 'message': biz_list['message']})
#
#
# def search_host(biz_id, condition):
#     params = {
#         "bk_app_code": APP_ID,
#         "bk_app_secret": APP_TOKEN,
#         "bk_username": "admin",
#         "bk_biz_id": biz_id,
#         "condition": condition
#     }
#     import json
#     change = "/api/c/compapi/v2/cc/search_host"
#     post_url = BK_PAAS_HOST + change
#     request = requests.post(post_url, json=params)
#     data = json.loads(request.content)
#     return data['data']['info']
#
#
# def get_host_tree(user, biz_id):
#     conditions = []
#     get_topo = get_biz_topology_by_id(user, biz_id)
#     if get_topo['result']:
#         topo = get_topo['data'][0]
#         for set in topo['child']:
#             condition = {"bk_obj_id": "set",
#                          "condition": [{"field": "bk_set_id", "operator": "$eq", "value": set['bk_inst_id']}]}
#             conditions.append(condition)
#             for module in set['child']:
#                 condition = {"bk_obj_id": "module",
#                              "condition": [{"field": "bk_module_id", "operator": "$eq", "value": module['bk_inst_id']}]}
#                 conditions.append(condition)
#                 host_list = search_host(biz_id, conditions)
#                 module['host'] = []
#                 for host in host_list:
#                     module['host'].append({'host_ip': host['host']['bk_host_innerip'],
#                                            'bk_cloud_id': host['host']['bk_cloud_id'][0]['bk_inst_id']})
#         host_tree = topo
#         return {'result': True, 'data': host_tree}
#     else:
#         return {'result': False, 'message': get_topo['message']}
#
#
# def html_escape(html):
#     html = html.replace('&quot;', '"')
#     html = html.replace('&amp;', '&')
#     html = html.replace('&lt;', '<')
#     html = html.replace('&gt;', '>')
#     html = html.replace('&nbsp;', ' ')
#     return html
#
#
# # 获取上传脚本内容
# def push_script(request):
#     file = request.FILES.get('files')
#     # file_name = str(file.name).decode('utf-8')
#     # file_suffix = os.path.splitext(file_name)[1][1:]
#     script_content = file.read()
#     script_content = html_escape(script_content)
#     return render_json({'result': True, 'data': script_content})
#
#
# # 删除上传脚本内容
# def delet_script(request):
#     return render_json({'result': True, 'data': ''})
#
#
# # 创建文件夹
# def mkdir(path):
#     folder = os.path.exists(path)
#     if not folder:
#         os.makedirs(path)
#
#
# # 文件分发，获取上传文件
# def push_file(request):
#     wkf_ins_id = request.GET.get('wkf_ins_id')
#     file = request.FILES.get('files')
#     file_name = file.name
#     # raise Exception("test")
#     date = datetime.datetime.now().strftime('%Y%m%d')
#     if RUN_MODE == 'DEVELOP':
#         path = 'C:\\uploadfile\\workflow_light\\' + 'wkf_ins_' + date + '_' + wkf_ins_id
#         mkdir(path)
#         file_path = path + '\\' + file_name
#     else:
#         path = '/opt/bksaas/uploadfile/workflow_light/' + 'wkf_ins_' + date + '_' + wkf_ins_id
#         mkdir(path)
#         file_path = path + '/' + file_name
#     if os.path.exists(file_path):
#         os.remove(file_path)
#     try:
#         file_obj = open(file_path, 'wb')
#         for chunk in file.chunks():
#             file_obj.write(chunk)
#         file_obj.close()
#     except Exception, e:
#         raise Exception(render_json({'result': False, 'message': '文件保存失败，请重新上传'}))
#     return render_json({'result': True, 'data': file_path})
#
#
# # 文件分发，删除文件
# def delet_file(request):
#     wkf_ins_id = request.GET.get('wkf_ins_id')
#     file_name = request.POST.get('fileNames')
#     file_name = html_escape(file_name)
#     date = datetime.datetime.now().strftime('%Y%m%d')
#     if RUN_MODE == 'DEVELOP':
#         path = 'C:\\uploadfile\\workflow_light\\' + 'wkf_ins_' + date + '_' + wkf_ins_id
#         mkdir(path)
#         file_path = path + '\\' + file_name
#     else:
#         path = '/opt/bksaas/uploadfile/workflow_light/' + 'wkf_ins_' + date + '_' + wkf_ins_id
#         mkdir(path)
#         file_path = path + '/' + file_name
#     if os.path.exists(file_path):
#         os.remove(file_path)
#     return render_json({'result': True, 'data': file_path})
#
#
# # 根据业务获取主机
# def get_host(request):
#     bk_biz_id = request.POST.get('bk_biz_id')
#     params = {
#         "bk_app_code": APP_ID,
#         "bk_app_secret": APP_TOKEN,
#         "bk_username": "admin",
#         "bk_biz_id": bk_biz_id}
#     change = "/api/c/compapi/v2/cc/search_host"
#     post_url = BK_PAAS_HOST + change
#     r = requests.post(post_url, json=params)
#     data = json.loads(r.content)
#     if data['result']:
#         host_list = []
#         num = data['data']['count']
#         for i in data['data']['info']:
#             host_list.append({'hostname': i['host']['bk_host_name'],
#                               'bk_os_name': i['host']['bk_os_name'],
#                               'bk_host_innerip': i['host']['bk_host_innerip'],
#                               'bk_cloud_name': i['host']['bk_cloud_id'][0]['bk_inst_name'],
#                               'bk_cloud_id': i['host']['bk_cloud_id'][0]['bk_inst_id']
#                               })
#         r = get_host_tree('admin', bk_biz_id)
#         host_tree = []
#         if r['result']:
#             host_tree = r['data']
#         return render_json({'result': True, 'data': {'count': num, 'data': host_list, 'host_tree': host_tree}})
#     return render_json({'result': False, 'message': data['message']})

import base64


def get_execute_script(content):
    script_content = base64.b64encode(content)
    url = BK_PAAS_HOST + '/api/c/compapi/v2/job/fast_execute_script/'
    params = {
        "bk_app_code": "test-frame",
        "bk_app_secret": "9038ca66-dd4c-4fff-ad6a-47b575d4588a",
        "bk_username": "admin",
        "bk_biz_id": 2,
        "script_content": script_content,
        "script_timeout": 1000,
        "account": "root",
        "is_param_sensitive": 0,
        "script_type": 1,
        "ip_list": [
            {
                "bk_cloud_id": 0,
                "ip": "192.168.165.84"
            }
        ]
    }
    r = requests.post(url, json=params, verify=False)
    data = json.loads(r.content)
    print data


def run():
    content = """
            #!/bin/bash
            MEMORY=$(free -m | awk 'NR==2{printf "%.2f%%", $3*100/$2 }')
            DISK=$(df -h | awk '$NF=="/"{printf "%s", $5}')
            CPU=$(top -bn1 | grep load | awk '{printf "%.2f%%", $(NF-2)}')
            DATE=$(date "+%Y-%m-%d %H:%M:%S")
            echo -e "$DATE|$MEMORY|$DISK|$CPU"
        """
    get_execute_script(content)

run()