# -*- coding: utf-8 -*-
import math

from django.http import HttpResponse, JsonResponse
from common.mymako import render_mako_context, render_json
from blueking.component.shortcuts import get_client_by_request, get_client_by_user
from conf.default import APP_ID, APP_TOKEN, BK_PAAS_HOST
from django.db.models import Q
from home_application.models import *
from helper import *
from other_helper import *

import datetime
import json


def home(request):
    """
    首页
    """
    return render_mako_context(request, '/home_application/js_factory.html')


def test(request):
    return JsonResponse({"result": True, "username": u"admin", "phone": u'18800000000',
                         'last_login': '2019-10-25 10:00:00', 'email': u'admin@qq.com'})


def get_app_list(request):
    client = get_client_by_request(request)
    result = get_business_by_user(client, request.user.username)
    return JsonResponse(result)


def get_business_by_user(client, username):
    kwargs = {
        "bk_app_code": APP_ID,
        "bk_app_secret": APP_TOKEN,
        "bk_username": username,
    }
    res = client.cc.search_business(**kwargs)
    if res["result"]:
        user_business_list = [{"bk_biz_id": i["bk_biz_id"], "bk_biz_maintainer": i["bk_biz_maintainer"],
                               "bk_biz_name": i["bk_biz_name"]} for i in res["data"]["info"]
                              if username in i["bk_biz_maintainer"].split(",")
                              ]
        return {"result": True, "data": user_business_list}
    else:
        return {"result": False, "data": res["data"]}


# 查询
def get_task_model(request):
    try:
        params = json.loads(request.body)
        name = params["name"]
        business = params["business"]
        type = params["type"]
        when_start = params["whenStart"] + " 00:00:00"
        when_end = params["whenEnd"] + " 23:59:59"

        pageSize = int(params["pageOption"]["pageSize"])
        pageNum = int(params["pageOption"]["pageNum"])
        pageOption = paging(pageSize, pageNum)

        task_obj = TaskModel.objects.filter(
            Q(business__contains=business) & Q(type__contains=type) & Q(name__contains=name) & Q(
                when_created__range=(when_start, when_end)))
        result_data = list(task_obj[pageOption[0]:pageOption[1]].values())
        allLen = task_obj.count()
        page_num = math.ceil(allLen / float(pageSize))
        return JsonResponse({'result': True, 'data': result_data, 'allLen': allLen, 'pageNum': page_num})
    except Exception, e:
        print e
        return JsonResponse({"result": False, "data": str(e)})


# 新增
def upload_file_form(request):
    try:
        params = json.loads(request.GET.get("params"))
        params["creator"] = request.user.username
        task_model = TaskModel.create_taskmodel(**params)
        error_list = up_excel(request, task_model)
        if len(error_list) > 0:
            return JsonResponse({'result': False, 'error': '、'.join(error_list)})
        return JsonResponse({"result": True})
    except Exception, e:
        print e
        return JsonResponse({"result": False, "data": str(e)})


# 删除
def delete_task_template(request):
    try:
        id = json.loads(request.body)["id"]
        task_obj = TaskModel.objects.get(id=id)
        task_obj.delete()
        return JsonResponse({"result": True})
    except Exception, e:
        print e
        return JsonResponse({"result": False, "data": str(e)})


# 修改
def update_task_template(request):
    try:
        params = json.loads(request.body)
        # TaskModel.objects.update_or_create(name=params["name"], business=params["business"], type=params["type"],
        #                                    creator=request.user.username,
        #                                    when_created=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        #                                    defaults={"id": params["id"]})  #不能用id做判断
        TaskModel.objects.filter(id=params["id"]).update(name=params["name"],
                                                       business=params["business"],
                                                       type=params["type"],
                                                       creator=request.user.username,
                                                       when_created=datetime.datetime.now().strftime(
                                                           "%Y-%m-%d %H:%M:%S"))
        return JsonResponse({"result": True})
    except Exception, e:
        print e
        return JsonResponse({"result": False, "data": str(e)})


def get_all_biz(request):
    client = get_client_by_request(request)
    result = client.cc.search_business()
    user_business_list = []
    if result['result']:
        user_business_list = [
            {"id": i["bk_biz_id"], "text": i["bk_biz_name"]} for i in result["data"]["info"]
        ]
    return JsonResponse({"result": True, "data": user_business_list})


def search_server_list(request):
    filter_obj = eval(request.body)
    bk_biz_id = request.GET["bk_biz_id"]
    client = get_client_by_request(request)
    if filter_obj["bk_obj_id"] not in ['biz', 'module', 'set']:
        filter_obj["bk_obj_id"] = 'object'
        condition = [{"field": "bk_inst_id", "operator": "$eq", "value": int(filter_obj["value"])}]
    else:
        condition = [{"field": "bk_%s_id" % filter_obj["bk_obj_id"], "operator": "$eq", "value": int(filter_obj["value"])}]
    kwargs = {
        'bk_biz_id': bk_biz_id,
        "condition": [
            {
                "bk_obj_id": filter_obj["bk_obj_id"],
                "fields": [],
                "condition": condition
            },
            {
                "bk_obj_id": "host",
                "fields": [],
                "condition": []
            }
        ]
    }
    result = client.cc.search_host(kwargs)
    return_data = [i['host'] for i in result["data"]["info"]]
    ip_list = [host['bk_host_innerip'] for host in return_data]
    ip_list = '\n'.join(ip_list)
    return render_json({"result": True, 'data': return_data, 'ipList': ip_list})


def get_app_topo(request):
    app_id = request.GET["app_id"]
    client = get_client_by_request(request)
    result = client.cc.search_biz_inst_topo({
        'bk_biz_id': app_id, 'level': -1
    })
    return_data = result["data"]
    for i in return_data:
        set_data = get_set_data(i)
        i["child"] = set_data
        format_is_parent(i)
    return render_json({"result": True, 'data': result["data"]})


def get_set_data(one_obj):
    return_data = []
    for i in one_obj["child"]:
        if i["bk_obj_id"] == 'set':
            return_data.append(i)
            continue
        else:
            return_data.extend(get_set_data(i))
    return return_data


def format_is_parent(i):
    i["isParent"] = len(i["child"]) > 0
    if i["isParent"]:
        for u in i["child"]:
            format_is_parent(u)


def get_host_attr(request):
    client = get_client_by_request(request)
    result = client.cc.search_object_attribute({'bk_obj_id': 'host'})
    return render_json({"result": True, 'data': [{'id': i['bk_property_id'], 'text': i['bk_property_name']} for i in result['data']]
                        })


def add_host_info(request):
    data = json.loads(request.body)
    biz = data['business']
    set_info = data['set']
    host = data['host']
    HostInfo.objects.create(biz=biz, set=set_info, host=host)
    return render_json({'result': True})


def get_host_info1(request):
    host_data = list(HostInfo.objects.all().values())
    return render_json({'result': True, 'data': host_data})