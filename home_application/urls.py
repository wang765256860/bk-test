# -*- coding: utf-8 -*-

from django.conf.urls import patterns
from home_application.host import host_url
from home_application.exam3 import back_url

urlpatterns = patterns(
    'home_application.views',
    (r'^$', 'home'),
    (r'^api/test/', 'test'),

    (r'^get_task_model$', 'get_task_model'),
    (r'^delete_task_template$', 'delete_task_template'),
    (r'^update_task_template$', 'update_task_template'),
    # 获取业务信息
    (r'^get_app_list$', 'get_app_list'),
    # excel文件
    (r'^upload_file_form/$', 'upload_file_form'),


    # 树形biz信息
    (r'^get_all_biz$', 'get_all_biz'),
    (r'^search_server_list$', 'search_server_list'),
    (r'^get_app_topo$', 'get_app_topo'),
    (r'^get_host_attr$', 'get_host_attr'),


    (r'^add_host_info$', 'add_host_info'),
    (r'^get_host_info1$', 'get_host_info1'),
)

urlpatterns += host_url.urlpatterns
urlpatterns += back_url.urlpatterns
