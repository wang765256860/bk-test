# -*- coding: utf-8 -*-
from django.db import models
import datetime


class TaskModel(models.Model):
    name = models.CharField(max_length=200, null=True)
    business = models.CharField(max_length=200, null=True)
    type = models.CharField(max_length=200, null=True)
    creator = models.CharField(max_length=200, null=True)
    when_created = models.CharField(max_length=100, null=True)

    @classmethod
    def create_taskmodel(cls, **kwargs):
        when_created = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        kwargs["when_created"] = when_created
        task_model = cls(**kwargs)
        task_model.save()
        return task_model


class Template(models.Model):
    task_model = models.ForeignKey(TaskModel)
    num = models.IntegerField(null=True)
    operation = models.CharField(max_length=300, null=True)
    desc = models.CharField(max_length=500, null=True)
    person = models.CharField(max_length=20, null=True)
    when_created = models.DateTimeField(null=True)


class HostInfo(models.Model):
    biz = models.CharField(max_length=20)
    set = models.CharField(max_length=20)
    host = models.CharField(max_length=20)


class BackRecord(models.Model):
    ip = models.CharField(max_length=20)
    file_list = models.CharField(max_length=50)
    file_num = models.CharField(max_length=50)
    file_size = models.CharField(max_length=50)
    when_backup = models.CharField(max_length=50)
    back_man = models.CharField(max_length=50)

