# -*- coding:utf-8 -*-
from django.conf.urls import patterns

urlpatterns = patterns(
    'home_application.exam3.back',
    (r'^execute_get_file_info$', 'execute_get_file_info'),
    (r'^execute_backup$', 'execute_backup'),
    (r'^get_backup_record$', 'get_backup_record'),
)
