import math

from django.http import HttpResponse, JsonResponse
from common.mymako import render_mako_context, render_json
from blueking.component.shortcuts import get_client_by_request, get_client_by_user
from conf.default import APP_ID, APP_TOKEN, BK_PAAS_HOST
from django.db.models import Q
from home_application.models import *
from home_application.esb_api import *
from home_application.helper import paging

import datetime
import json


def execute_get_file_info(request):
    content = """
        #!/bin/bash
        cd /data/logs
        du -ah ./
        ls -lR | grep .txt|wc -l
        """
    data = json.loads(request.body)['ip_list']
    bk_biz_id = request.GET.get('bk_biz_id', '')
    ip_list = []
    for i in data:
        ip_info = dict()
        ip_info['ip'] = i['ip']
        ip_info['bk_cloud_id'] = i['bk_cloud_id']
        ip_list.append(ip_info)
    result = excute_by_script(request.user.username, bk_biz_id, ip_list, content)
    return_data = []
    for my_data in result['data']:
        data_info = dict()
        data_info['ip'] = my_data['ip']
        log_content = my_data['log_content'].strip().split('\n')
        data_info['num'] = log_content[-1]
        file_info = log_content[:int(data_info['num'])]
        data_info['file_name'] = ';'.join([file.split('\t./')[1] for file in file_info])
        data_info['size'] = sum([float(file1.split('K\t')[0]) for file1 in file_info])
        return_data.append(data_info)
    return render_json({'result': True, 'data': return_data})


def execute_backup(request):
    data = json.loads(request.body)
    backer = 'wangzhen'
    now = str(datetime.datetime.now()).split(".")[0]
    if BackRecord.objects.filter(ip=data['ip']).exists():
        BackRecord.objects.filter(ip=data['ip']).update(**data)
    BackRecord.objects.create(when_backup=now,back_man=backer, **data)
    return render_json({'result': True})


def get_backup_record(request):
    page_size = int(json.loads(request.body)["pageOption"]["pageSize"])
    page_num = int(json.loads(request.body)["pageOption"]["pageNum"])
    page_option = paging(page_size, page_num)
    data = list(BackRecord.objects.all()[page_option[0]:page_option[1]].values())
    all_len = BackRecord.objects.all().count()
    page_num = math.ceil(all_len / float(page_size))
    return render_json({'result': True, 'data': data, "allLen": all_len, "pageNum": page_num})