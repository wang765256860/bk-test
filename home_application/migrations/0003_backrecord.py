# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home_application', '0002_hostinfo'),
    ]

    operations = [
        migrations.CreateModel(
            name='BackRecord',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ip', models.CharField(max_length=20)),
                ('file_list', models.CharField(max_length=50)),
                ('file_num', models.CharField(max_length=50)),
                ('file_size', models.CharField(max_length=50)),
                ('when_backup', models.CharField(max_length=50)),
                ('back_man', models.CharField(max_length=50)),
            ],
        ),
    ]
