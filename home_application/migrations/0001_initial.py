# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TaskModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, null=True)),
                ('business', models.CharField(max_length=200, null=True)),
                ('type', models.CharField(max_length=200, null=True)),
                ('creator', models.CharField(max_length=200, null=True)),
                ('when_created', models.CharField(max_length=100, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Template',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('num', models.IntegerField(null=True)),
                ('operation', models.CharField(max_length=300, null=True)),
                ('desc', models.CharField(max_length=500, null=True)),
                ('person', models.CharField(max_length=20, null=True)),
                ('when_created', models.DateTimeField(null=True)),
                ('task_model', models.ForeignKey(to='home_application.TaskModel')),
            ],
        ),
    ]
