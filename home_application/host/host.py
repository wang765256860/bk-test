import math

from django.http import HttpResponse, JsonResponse
from common.mymako import render_mako_context, render_json
from blueking.component.shortcuts import get_client_by_request, get_client_by_user
from conf.default import APP_ID, APP_TOKEN, BK_PAAS_HOST
from django.db.models import Q
from home_application.models import *
from home_application.esb_api import *

import datetime
import json


def get_host_by_biz(request):
    client = get_client_by_request(request)
    ip = json.loads(request.body)['ip']
    ip_list = ip.split('\n')
    biz_id = int(request.GET.get('biz_id', ''))
    kwargs = {
        'bk_biz_id': biz_id,
        "ip": {
            "data": ip_list,
            "exact": 1,
            "flag": "bk_host_innerip|bk_host_outerip"
        },
        "condition": [
            {
                "bk_obj_id": 'biz',
                "fields": [],
                "condition": [{
                       "field": 'bk_biz_id',
                       'operator': '$eq',
                       'value': biz_id
                   }]
            }
        ]
    }
    result = client.cc.search_host(kwargs)
    res = []
    for data in result["data"]["info"]:
        return_data = dict()
        return_data['inner_ip'] = data['host']['bk_host_innerip']
        return_data['os'] = data['host']['bk_os_name']
        return_data['host_name'] = data['host']['bk_host_name']
        return_data['mem'] = ''
        return_data['disk'] = ''
        return_data['cpu'] = ''
        return_data['cloud_area_name'] = data['host']['bk_cloud_id'][0]['bk_inst_name'] if data['host']['bk_cloud_id'] else ''
        return_data['cloud_area_id'] = data['host']['bk_cloud_id'][0]['bk_inst_id'] if data['host']['bk_cloud_id'] else ''
        res.append(return_data)
    return render_json({"result": True, 'data': res})


def excute_search_script(request):
    content = """
    #!/bin/bash
    MEMORY=$(free -m | awk 'NR==2{printf "%.2f%%", $3*100/$2 }')
    DISK=$(df -h | awk '$NF=="/"{printf "%s", $5}')
    CPU=$(top -bn1 | grep load | awk '{printf "%.2f%%", $(NF-2)}')
    DATE=$(date "+%Y-%m-%d %H:%M:%S")
    echo -e "$DATE|$MEMORY|$DISK|$CPU"
    """
    data = json.loads(request.body)
    ip = data['ip']
    bk_cloud_id = data['bk_cloud_id']
    bk_biz_id = data['biz_id']
    ip_list = [{'ip': ip, 'bk_cloud_id': bk_cloud_id}]
    result = excute_by_script(request.user.username, bk_biz_id, ip_list, content)
    log = result['data'][0]['log_content'].strip().split('|')
    res = {'mem': log[1], 'disk': log[2], 'cpu': log[3]}
    return render_json({'result': True, 'data': res})

