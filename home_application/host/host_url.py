# -*- coding:utf-8 -*-
from django.conf.urls import patterns

urlpatterns = patterns(
    'home_application.host.host',
    (r'^get_host_by_biz$', 'get_host_by_biz'),
    (r'^excute_search_script$', 'excute_search_script'),
)
